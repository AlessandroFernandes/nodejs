var express = require("express");
var load = require("express-load");

module.exports = function () {
    var app = express();
    app.set("view engine", "ejs");
    app.set("views", "./app/views");
    load('route', {cwd: 'app'})
        .then('infra')
        .into(app);
    return app;
}

//git remote add origin https://AlessandroFernandes@bitbucket.org/AlessandroFernandes/nodejs.git
