var mysql = require('mysql');

function loadDb() {
    return mysql.createConnection({
        host: 'localhost',
        user: 'teste',
        password: 'root',
        database: 'teste'
    })

}

module.exports = function(){
    return loadDb;
}